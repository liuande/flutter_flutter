import 'package:anguo/utils/key_utlis.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flutter/generated/l10n.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './my_app_routes.dart' show kAppRoutingTable;
import './my_app_settings.dart';
import './themes.dart';

class MyMainApp extends StatelessWidget {
  final SharedPreferences sharedPref;

  const MyMainApp(this.sharedPref, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MyAppSettings>.value(
      value: MyAppSettings(sharedPref),
      child: const _MyMaterialApp(),
    );
  }
}

class _MyMaterialApp extends StatelessWidget {
  const _MyMaterialApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      localizationsDelegates: const [
        S.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      // 设置中文为首选项
      supportedLocales: [
        const Locale('zh', ''),
        ...S.delegate.supportedLocales
      ],
      theme: Provider.of<MyAppSettings>(context).isDarkMode
          ? kDarkTheme
          : kLightTheme,
      routes: kAppRoutingTable,
      navigatorKey: KeyUtils.globalKey,
      builder: EasyLoading.init(),
    );
  }
}
