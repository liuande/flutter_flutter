import 'package:anguo/utils/key_utlis.dart';
import 'package:flutter/material.dart';
import 'package:flutter_flutter/generated/l10n.dart';
import 'package:url_launcher/url_launcher.dart' as url_launcher;

import 'package:flutter_flutter/constants.dart';

// Inspired by the about page in Eajy's flutter demo:
// https://github.com/Eajy/flutter_demo/blob/master/lib/route/about.dart
class MyAboutRoute extends StatelessWidget {
  const MyAboutRoute({Key? key}) : super(key: key);

  // These tiles are also used as drawer nav items in home route.
  static final List<Widget> kAboutListTiles = <Widget>[
    const ListTile(
      title: Text(APP_DESCRIPTION),
    ),
    const Divider(),


  ];

  @override
  Widget build(BuildContext context) {
    final header = ListTile(
      leading: kAppIcon,
      title:  Text(S.of(KeyUtils.context).app_name),
      subtitle:  Text(APP_VERSION),
      trailing: IconButton(
        icon: const Icon(Icons.info),
        onPressed: () {
          showAboutDialog(
              context: context,
              applicationName: S.of(KeyUtils.context).app_name,
              applicationVersion: APP_VERSION,
              applicationIcon: kAppIcon,
              children: <Widget>[const Text(APP_DESCRIPTION)]);
        },
      ),
    );
    return ListView(
      children: <Widget>[
        header,
        ...kAboutListTiles,
      ],
    );
  }
}
