// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("About"),
        "advanced": MessageLookupByLibrary.simpleMessage("Advanced"),
        "app_name": MessageLookupByLibrary.simpleMessage("Flutter tutorial"),
        "basics": MessageLookupByLibrary.simpleMessage("Basics"),
        "bookmarks": MessageLookupByLibrary.simpleMessage("Bookmarks"),
        "code": MessageLookupByLibrary.simpleMessage("Code"),
        "copy_code_desc":
            MessageLookupByLibrary.simpleMessage("Copy code link to clipboard"),
        "copy_code_ok_desc": MessageLookupByLibrary.simpleMessage(
            "Code link copied to clipboard!"),
        "dark_mode": MessageLookupByLibrary.simpleMessage("Dark mode"),
        "off": MessageLookupByLibrary.simpleMessage("off"),
        "on": MessageLookupByLibrary.simpleMessage("on"),
        "preview": MessageLookupByLibrary.simpleMessage("Preview"),
        "view_code_in_browser":
            MessageLookupByLibrary.simpleMessage("View code in browser"),
        "zoom_in": MessageLookupByLibrary.simpleMessage("Zoom in"),
        "zoom_out": MessageLookupByLibrary.simpleMessage("Zoom out")
      };
}
