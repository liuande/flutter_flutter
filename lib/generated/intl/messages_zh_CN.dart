// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh_CN locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh_CN';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "about": MessageLookupByLibrary.simpleMessage("关于"),
        "advanced": MessageLookupByLibrary.simpleMessage("高级"),
        "app_name": MessageLookupByLibrary.simpleMessage("Flutter 教程"),
        "basics": MessageLookupByLibrary.simpleMessage("基础"),
        "bookmarks": MessageLookupByLibrary.simpleMessage("收藏"),
        "code": MessageLookupByLibrary.simpleMessage("代码"),
        "copy_code_desc": MessageLookupByLibrary.simpleMessage("将代码链接复制到剪贴板"),
        "copy_code_ok_desc":
            MessageLookupByLibrary.simpleMessage("代码链接已复制到剪贴板！"),
        "dark_mode": MessageLookupByLibrary.simpleMessage("黑暗模式"),
        "off": MessageLookupByLibrary.simpleMessage("关"),
        "on": MessageLookupByLibrary.simpleMessage("开"),
        "preview": MessageLookupByLibrary.simpleMessage("预览"),
        "view_code_in_browser":
            MessageLookupByLibrary.simpleMessage("在浏览器中查看代码"),
        "zoom_in": MessageLookupByLibrary.simpleMessage("放大"),
        "zoom_out": MessageLookupByLibrary.simpleMessage("缩小")
      };
}
