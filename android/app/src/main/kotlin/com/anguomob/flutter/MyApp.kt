package com.anguomob.flutter
import androidx.multidex.MultiDexApplication
import com.anguomob.total.Anguo
import com.anguomob.total.BuildConfig
class MyApp : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        Anguo.init(this, BuildConfig.DEBUG)
    }
}